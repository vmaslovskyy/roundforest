package roundforest;

import java.io.File;
import java.util.List;
import java.util.concurrent.ExecutionException;

import roundforest.entity.Review;
import roundforest.service.CSVHandler;
import roundforest.service.StatisticsPrinter;

public class Application {

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		
		if (args.length == 0) {
			System.out.println("No CSV file was provided!");
			return;
		}
	    String inPath = args[0];
	    
	    
	    File file = new File(inPath);
		if (!file.exists()) {
			System.out.println("CSV file is not found!");
			return;
		}
		
	    int limit = 1000;

		List<Review> reviews = new CSVHandler().handle(inPath);
		StatisticsPrinter.print(reviews, limit);
		
	}
	
}

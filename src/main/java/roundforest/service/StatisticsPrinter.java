package roundforest.service;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import roundforest.entity.Review;

public class StatisticsPrinter {
	
	public static void print(List<Review> reviews, int limit) throws InterruptedException, ExecutionException {
		
		CompletableFuture<List<String>> futureMostActiveUsers  
		  = CompletableFuture.supplyAsync(() -> new ReviewAnalyzer().getMostActiveUsers(reviews, limit));
		CompletableFuture<List<String>> futureMostCommentedFoodItems  
		  = CompletableFuture.supplyAsync(() -> new ReviewAnalyzer().getMostCommentedFoodItems(reviews, limit));
		CompletableFuture<List<String>> futureMostUsedWords  
		  = CompletableFuture.supplyAsync(() -> new ReviewAnalyzer().getMostUsedWords(reviews, limit));
		 
		CompletableFuture.allOf(futureMostActiveUsers, futureMostCommentedFoodItems, futureMostUsedWords);

		System.out.println("\nMost active users are:");
		futureMostActiveUsers.get()
			.stream()
        	.sorted((o1, o2) -> String.CASE_INSENSITIVE_ORDER.compare(o1, o2))
        	.forEachOrdered(profileName -> System.out.println(profileName));

		System.out.println("\nMost commented food items are:");
		futureMostCommentedFoodItems.get()
			.stream()
	    	.sorted((o1, o2) -> String.CASE_INSENSITIVE_ORDER.compare(o1, o2))
			.forEachOrdered(foodItem -> System.out.println(foodItem));

		System.out.println("\nMost used words are:");
		futureMostUsedWords.get()
			.stream()
	    	.sorted((o1, o2) -> String.CASE_INSENSITIVE_ORDER.compare(o1, o2))
			.forEachOrdered(word -> System.out.println(word));
	}
	
}

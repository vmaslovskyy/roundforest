package roundforest.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import roundforest.entity.Review;

public class ReviewAnalyzer {
	
	public static final String WORD_SEPARATOR = "\\W+";
	private Pattern wordPettern = Pattern.compile("[a-zA-Z]*");

	public List<String> getMostActiveUsers(List<Review> reviewsList, int limit){
		Map<String, Integer> map = new HashMap<>();
		reviewsList.forEach(reviw -> putKey(map, reviw.getProfileName()));
		return sortAndLimit(map, limit);
		
	}
	public List<String> getMostCommentedFoodItems(List<Review> reviewsList, int limit){
		Map<String, Integer> map = new HashMap<>();
		reviewsList.forEach(reviw -> putKey(map, reviw.getProductId()));
		return sortAndLimit(map, limit);
		
	}
	public List<String> getMostUsedWords(List<Review> reviewsList, int limit) {
		Map<String, Integer> map = new HashMap<>();
		reviewsList.forEach(reviw -> {
			
			for (String word : getAllWordsFromReview(reviw)) {
				if(StringUtils.isNotEmpty(word) && wordPettern.matcher(word.trim()).matches()) { ////?????
					putKey(map, word.trim().toLowerCase());
				}
	        }
		});
		return sortAndLimit(map, limit);
	}
	
	private void putKey(Map<String, Integer> map, String key) {
	      Integer count = map.get(key);
	      map.put(key, count == null ? 1 : count++);
	  }
		
	private List<String> sortAndLimit(Map<String, Integer> map, int limit) {
	      return map.entrySet()
	              .stream()
	              .sorted((o1, o2) -> Integer.compare(o2.getValue(), o1.getValue()))
	              .limit(limit)
	              .map(Map.Entry::getKey)
	              .collect(Collectors.toList());
	}
		
	private String[] getAllWordsFromReview(Review reviw) {
		return ArrayUtils.addAll(
				reviw.getSummary().split(WORD_SEPARATOR),
              reviw.getText().split(WORD_SEPARATOR));
	}
		
}

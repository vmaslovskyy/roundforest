package roundforest.service;

import akka.Done;
import akka.actor.ActorSystem;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.FileIO;
import akka.stream.javadsl.Framing;
import akka.stream.javadsl.FramingTruncation;
import akka.stream.javadsl.Sink;
import akka.util.ByteString;
import roundforest.entity.Review;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;


public class CSVHandler {
	
	public static final String CSV_SEPARATOR = ",";
	
	private static Review getReview(String[] csvRecord) {
		return new Review.ReviewBuilder()
				.profileName(csvRecord[3])
				.userId(csvRecord[2])
				.productId(csvRecord[1])
				.summary(csvRecord[8])
				.text(csvRecord[9])
				.build();
	}
	
	public List<Review> handle(String inPath) throws InterruptedException, ExecutionException {
		  
		final ActorSystem system = ActorSystem.create("Sys");
		final ActorMaterializer materializer = ActorMaterializer.create(system);
		final Path file = Paths.get(inPath);
		List<Review> reviewsList = new ArrayList<>();
		
		Sink<ByteString, CompletionStage<Done>> getReviewsSink =
			    Sink.<ByteString> foreach(chunk -> {
			    	String[] csvRecord = chunk.utf8String().split(CSV_SEPARATOR);
					reviewsList.add(getReview(csvRecord));
			    });
		
		CompletableFuture<List<Review>> future = FileIO.fromPath(file)
		  // parse bytestrings (chunks of data) to lines
		  .via(Framing.delimiter(ByteString.fromString("\n"), 1024 * 1024, FramingTruncation.DISALLOW))
		  .to(getReviewsSink)
		  .run(materializer)
		  .handle((ioResult, failure) -> {
		      if (failure != null) System.err.println("Failure: " + failure);
		      else if (!ioResult.wasSuccessful()) System.err.println("Handling failed " + ioResult.getError());
		      else System.out.println("Successfully handled " + ioResult.getCount() + " bytes");
			      system.terminate();
			      System.out.println("Reviews count " + reviewsList.size());
			      return reviewsList;
			   }).toCompletableFuture();
			
			return future.get();
					
	}
	
}

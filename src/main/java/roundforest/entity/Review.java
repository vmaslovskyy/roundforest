package roundforest.entity;

public class Review {
	
	private String productId;
	private String userId;
	private String profileName;
	private String summary;
	private String text;
	
	private Review() {
	}

	public String getProductId() {
		return productId;
	}


	public void setProductId(String productId) {
		this.productId = productId;
	}


	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getProfileName() {
		return profileName;
	}

	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((productId == null) ? 0 : productId.hashCode());
		result = prime * result + ((profileName == null) ? 0 : profileName.hashCode());
		result = prime * result + ((summary == null) ? 0 : summary.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Review other = (Review) obj;
		if (productId == null) {
			if (other.productId != null)
				return false;
		} else if (!productId.equals(other.productId))
			return false;
		if (profileName == null) {
			if (other.profileName != null)
				return false;
		} else if (!profileName.equals(other.profileName))
			return false;
		if (summary == null) {
			if (other.summary != null)
				return false;
		} else if (!summary.equals(other.summary))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}


	public static class ReviewBuilder {
		
		private String productId;
		private String userId;
		private String profileName;
		private String summary;
		private String text;
		
		public Review build() {
	
			Review review = new Review();
			review.productId = this.productId;
			review.userId = this.userId;
			review.profileName = this.profileName;
			review.summary = this.summary;
			review.text = this.text;
			return review;
		}
		 
		public ReviewBuilder productId(String productId) {
            this.productId = productId;
            return this;
        }

        public ReviewBuilder userId(String userId) {
            this.userId = userId;
            return this;
        }

        public ReviewBuilder profileName(String profileName) {
            this.profileName = profileName;
            return this;
        }
        
        public ReviewBuilder summary(String summary) {
            this.summary = summary;
            return this;
        }

        public ReviewBuilder text(String text) {
            this.text = text;
            return this;
        }
	}
}
